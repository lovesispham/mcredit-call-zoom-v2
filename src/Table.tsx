import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import Pagination from "./Pagination";
import ReactPlayer from "react-player";
import Draggable from "react-draggable"; // The default
import Slide from "./SlideWrapper";
import Spinner from "./Spinner";
import useOutsideClick from "./useOutsideClick";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { saveAs } from 'file-saver';

const Table: React.FC = () => {
  const myInfo = JSON.parse(String(localStorage.getItem('myinfo')));

  const [loading, setLoading] = useState<boolean>(false);
  const [loadingVideo, setLoadingVideo] = useState<boolean>(false);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(10);
  const [dataPagination, setDataPagination] = useState({
    offset: 0,
    limit: 10,
  });
  const [data, setData] = useState<[]>([]);
  const [openModalVideo, setOpenModalVideo] = useState<boolean>(false);
  const [itemVid, setItem] = useState<string>("");
  const [dataCloud, setDataCloud] = useState<string>("");

  const [openModalSlide, setOpenModalSlide] = useState<boolean>(false);
  const [openModalExport, setOpenModalExport] = useState<boolean>(false);
  const [itemSlide, setItemSlide] = useState<any>("");

  const [search, setSearch] = useState<{
    from: null;
    to: null;
    dst: string;
    src: string;
    code: string;
  }>({
    from: null,
    to: null,
    dst: "",
    src: "",
    code: "",
  });

  var today = new Date();

  const [exportData, setExportData] = useState<{
    start: Date | any;
    end: Date | any;
    name: string;
    phone: string;
  }>({
    start: new Date(today.getFullYear(), today.getMonth(), 1),
    end: new Date(today.getFullYear(), today.getMonth() + 1, 0),
    name: "",
    phone: "",
  });

  const handleChangeInput = (e: any) => {
    const { name, value } = e.target;
    setSearch({ ...search, [name]: value });
  };

  const handleChange = (date: any, name: any) => {
    setSearch({
      ...search,
      [name]: date,
    });
  };

  const handleChangeInputExport = (e: any) => {
    const { name, value } = e.target;
    setExportData({ ...exportData, [name]: value });
  };

  const handleChangeExport = (date: any, name: any) => {
    setExportData({
      ...exportData,
      [name]: date,
    });
  };

  const searchRef = useRef(null);
  const [openSearch, setOpenSearch] = useState(false);
  useOutsideClick(searchRef, () => {
    if (openSearch) setOpenSearch(false);
  });

  const [percent, setPercent] =  useState<{
    loading: number;
    show:boolean;
  }>({
    loading:0,
    show:false
  });

  let token = localStorage.getItem("access_token");
  let axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  };

  let axiosConfigDownload = {
    
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
    responseType: 'blob' as any,
    onDownloadProgress: (progressEvent:any) => {
      var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      setPercent({
          loading: percentCompleted,
          show:true
      })
  },
  }

  const handlePrevPage = (prevPage: number) => {
    setPage((prevPage) => prevPage - 1);
    let data = {
      offset: (page - 1 - 1) * dataPagination.limit,
      limit: 10,
    };
    setDataPagination(data);
  };

  const handleNextPage = (nextPage: number) => {
    setPage((nextPage) => nextPage + 1);
    let data = {
      offset: page * dataPagination.limit,
      limit: 10,
    };
    setDataPagination(data);
  };

  const fetchData = async () => {
    let params = `limit=${dataPagination.limit}&offset=${dataPagination.offset}`;
    axios
      .get(`${process.env.REACT_APP_API_URL}api/history?${params}`, axiosConfig)
      .then((res) => {
        setLoading(true);

        setTimeout(() => {
          setLoading(false);
        }, 500);
        setData(res.data.data.list_cdr);
        setTotalPages(res.data.data.sms_count);

        // getLocationCustomer()
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleSearch = () => {
    let params = "";
    params +=
      search.code !== "" && search.code !== undefined
        ? `code=${search.code}&`
        : "";
    params +=
      search.dst !== "" && search.dst !== undefined ? `dst=${search.dst}&` : "";
    params +=
      search.src !== "" && search.src !== undefined ? `src=${search.src}&` : "";
    params +=
      search.from !== null && search.from !== undefined
        ? `from=${moment(search.from).format("DD/MM/YYYY")}&`
        : "";
    params +=
      search.to !== null && search.to !== undefined
        ? `to=${moment(search.to).format("DD/MM/YYYY")}&`
        : "";
    // params += `limit=${dataPagination.limit}&offset=0`;
    const fetchData = async () => {
      axios
        .get(
          `${process.env.REACT_APP_API_URL}api/history?${params}`,
          axiosConfig
        )
        .then((res) => {
          setData(res.data.data.list_cdr);
          setTotalPages(res.data.data.sms_count);
          setLoading(true);

          setTimeout(() => {
            setLoading(false);
          }, 500);
          // getLocationCustomer()
        })
        .catch((error) => {
          console.error(error);
        });
    };
    fetchData();
  };

  const handleExport = async() =>{
    setPercent({
      ...percent,
      show:true
  })
        let params = "";
        params +=
          exportData.phone !== "" && exportData.phone !== undefined
            ? `phone=${exportData.phone}&`
            : "";
        params +=
          exportData.name !== "" && exportData.name !== undefined ? `name=${exportData.name}&` : "";
        params +=
        exportData.start !== null && exportData.start !== undefined
            ? `start=${moment(exportData.start).format("YYYY-MM-DD")}&`
            : "";
        params +=
        exportData.end !== null && exportData.end !== undefined
            ? `end=${moment(exportData.end).format("YYYY-MM-DD")}&`
            : "";
        axios
        .get(
          `${process.env.REACT_APP_API_URL}api/download/history?${params}`,
          axiosConfigDownload
          
        )
        .then((res) => {
          console.log(res)
          // getLocationCustomer()
          let blob = new Blob([res.data], { type: 
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
           });
           let datetime = "";
           datetime +=exportData.end !== null && exportData.end !== undefined
           ? `end_${moment(exportData.end).format("YYYY-MM-DD")}_`
           : "";
           datetime += exportData.start !== null && exportData.start !== undefined
           ? `start_${moment(exportData.start).format("YYYY-MM-DD")}`
           : "";
           saveAs(blob, `result_${datetime}.xlsx`);
            setPercent({
              ...percent,
              show:false,
        })
        setOpenModalExport(false)
        })
        .catch((error) => {
          console.error(error);
        });
  }


  const handleClear = () => {
    setSearch({
      from: null,
      to: null,
      dst: "",
      src: "",
      code: "",
    });
    fetchData();
    if (openSearch) setOpenSearch(false);
  };

  const handleOpenModalVideo = () => {
    setOpenModalVideo(true);
    setTimeout(() => {
      setLoadingVideo(false);
    }, 2000);
  };

  const handleCloseModal = () => {
    setOpenModalVideo(false);
    setDataCloud("");
    setItem("");
  };

  const handleCloseModalExport = () => {
    setOpenModalExport(false)
  }

  const handleOpenModalSlide = () => {
    setOpenModalSlide(true);
  };

  const handleCloseModalSlide = () => {
    setOpenModalSlide(false);
  };

  // data cloud
  const getDataCloud = async (id: any) => {
    axios
      .get(
        `${process.env.REACT_APP_API_URL}api/history-cloud/${id}`,
        axiosConfig
      )
      .then((res) => {
        if (res.status === 200) {
          setDataCloud(res.data.Data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  function dateCloud(date: string) {
    let convertDateCloud = moment(date).format("YYYY-MM-DD");
    let a = moment().diff(convertDateCloud, "days");
    return a;
  }

  return (
    <>
      {openModalVideo && (
        <>
          {loadingVideo ? (
            <>
              <div className="modal-overlay"></div>
              <div className="modal-popup_style2 open">
                <div className="popup-container">
                  <div className="dot-pulse"></div>
                </div>
              </div>
            </>
          ) : (
            <>
              <Draggable>
                <div className="modal-popup_style2 open">
                  <div className="popup-container pu-video">
                    <div
                      className="popup-close"
                      onClick={() => handleCloseModal()}
                    >
                      <span className="fa fa-close"></span>
                    </div>
                    <div className="content">
                      <ReactPlayer
                        url={
                          dataCloud
                            ? dataCloud
                            : `${process.env.REACT_APP_API_URL}${itemVid}`
                        }
                        playing
                        controls
                      />
                    </div>
                  </div>
                </div>
              </Draggable>
            </>
          )}
        </>
      )}

      {openModalSlide && (
        <>
          <div className="modal-overlay"></div>
          <div className="modal-popup_style2 open">
            <div className="popup-container pu-slide">
              <div
                className="popup-close"
                onClick={() => handleCloseModalSlide()}
              >
                <span className="fa fa-close"></span>
              </div>
              <div className="content">
                <Slide itemSlide={itemSlide} />
              </div>
            </div>
          </div>
        </>
      )}

      {openModalExport && myInfo.isAdmin === true && (
        <>
          <div className="modal-overlay"></div>
          <div className="modal-popup_style2 open">
            <div className="popup-container pu-export">
              <div className="popup-close" onClick={()=>handleCloseModalExport()}>
                <span className="fa fa-close"></span>
              </div>
              <div className="content">
                {
                 percent.show && (
                  <Spinner 
                  heading={'Đang tải xuống'} 
                  percentText={percent.loading}
                  />
                 )
                  
                }
                <div className="form-input">
                  <span className="label-name">
                    Ngày bắt đầu - Ngày kết thúc
                  </span>
                  <div className="box-date">
                    <DatePicker
                      autoComplete="off"
                      id="from"
                      name="from"
                      selected={exportData.start}
                      dateFormat="dd/MM/yyyy"
                      onChange={(date) => handleChangeExport(date, "start")}
                    />
                    <DatePicker
                      autoComplete="off"
                      id="to"
                      name="to"
                      selected={exportData.end}
                      dateFormat="dd/MM/yyyy"
                      onChange={(date) => handleChangeExport(date, "end")}
                    />
                  </div>
                </div>
                <div className="box-date">
                  <div className="form-input" style={{ marginRight: 10 }}>
                    <span className="label-name">SĐT</span>
                    <input
                      name="phone"
                      type="text"
                      className="form-control"
                      value={exportData.phone}
                      onChange={handleChangeInputExport}
                    />
                  </div>
                  <div className="form-input">
                    <span className="label-name">Nguồn</span>
                    <input
                      name="name"
                      type="text"
                      className="form-control"
                      value={exportData.name}
                      onChange={handleChangeInputExport}
                    />
                  </div>
                  
                </div>
                <div className="form-input text-right">
                
                <div className="btn" onClick={() => handleExport()}>
                  Tải xuống
                </div>
              </div>
              </div>
            </div>
          </div>
        </>
      )}
      <div className="container table-report">
        <div className={`${myInfo.isAdmin === true ? 'top-filter' : ''}`}>
          {
            myInfo.isAdmin === true && (
              <span
              className="btn-filter"
              onClick={() => setOpenModalExport(!openModalExport)}
            >
              <i className="fa fa-download"></i>
            </span>
            )
          }
          <div className="search-wrapper" ref={searchRef}>
            <span
              className="btn-filter"
              onClick={() => setOpenSearch(!openSearch)}
            >
              <i className="fa fa-filter"></i>
            </span>
            <div className={`box-search ${openSearch ? "active" : ""}`}>
              <div className="form-input">
                <span className="label-name">Ngày bắt đầu - Ngày kết thúc</span>
                <div className="box-date">
                  <DatePicker
                    autoComplete="off"
                    id="from"
                    name="from"
                    selected={search.from}
                    dateFormat="dd/MM/yyyy"
                    onChange={(date) => handleChange(date, "from")}
                  />
                  <DatePicker
                    autoComplete="off"
                    id="to"
                    name="to"
                    selected={search.to}
                    dateFormat="dd/MM/yyyy"
                    onChange={(date) => handleChange(date, "to")}
                  />
                </div>
              </div>
              <div className="box-date">
                <div className="form-input" style={{ marginRight: 10 }}>
                  <span className="label-name">SĐT</span>
                  <input
                    name="dst"
                    type="text"
                    className="form-control"
                    value={search.dst}
                    onChange={handleChangeInput}
                  />
                </div>
                <div className="form-input">
                  <span className="label-name">Nguồn</span>
                  <input
                    name="src"
                    type="text"
                    className="form-control"
                    value={search.src}
                    onChange={handleChangeInput}
                  />
                </div>
              </div>
              <div className="form-input">
                <span className="label-name">Code</span>
                <input
                  name="code"
                  type="text"
                  className="form-control"
                  value={search.code}
                  onChange={handleChangeInput}
                />
              </div>
              <div className="form-input text-right">
                <div
                  className="btn"
                  style={{ marginRight: 10 }}
                  onClick={() => handleClear()}
                >
                  Xoá bộ lọc
                </div>
                <div className="btn" onClick={() => handleSearch()}>
                  Tìm kiếm
                </div>
              </div>
            </div>
          </div>
        </div>

        {loading ? <Spinner /> : null}
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>STT</th>
                <th>Nguồn</th>
                <th>Đích</th>
                <th>Mã số</th>
                <th>Đánh giá</th>
                <th>Nội dung</th>
                {/* <th>Đã xử lý</th> */}
                <th>File ghi</th>
                <th>Dung lượng</th>
                <th>Tổng thời gian</th>
                <th>Ngày bắt đầu</th>
                <th>Ngày kết thúc</th>
                <th>Tệp đính kèm</th>
                <th>Đánh giá của KH</th>
                <th>Nội dung</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((item: any, index: number) => {
                  return (
                    <tr key={index}>
                      <th style={{ textAlign: "center" }}>{index + 1} </th>
                      <th>{item.src} </th>
                      <th>{item.dst}</th>
                      <th>{item.code} </th>
                      <th>{item.feedback_type} </th>
                      <th>{item.description} </th>
                      <th>
                        {dateCloud(item?.recording_start) < 2 ? (
                          <>
                            <button
                              className="btn-play"
                              onClick={() => {
                                handleOpenModalVideo();
                                setLoadingVideo(true);
                                setItem(item.url);
                              }}
                            >
                              <i className="fa fa-play"></i>
                            </button>
                          </>
                        ) : (
                          <>
                            {item.file_size === "0" ? (
                              "Đang xử lý"
                            ) : (
                              <button
                                className="btn-play"
                                onClick={() => {
                                  handleOpenModalVideo();
                                  setLoadingVideo(true);
                                  getDataCloud(item.id);
                                }}
                              >
                                <i className="fa fa-cloud-download"></i>
                              </button>
                            )}
                          </>
                        )}
                      </th>
                      <th>
                        {item.file_size === "0" ? "Đang xử lý" : item.file_size}{" "}
                      </th>
                      <th>{item.time_record} </th>
                      <th>{item.recording_start} </th>
                      <th>{item.recording_end} </th>

                      <th>
                        <span
                          style={{ color: "#00b4fa", cursor: "pointer" }}
                          onClick={() => {
                            handleOpenModalSlide();
                            setItemSlide(item?.images);
                          }}
                        >
                          Xem tệp
                        </span>
                      </th>
                      <th>{item.feedback_customer} </th>
                      <th>{item.description_customer} </th>
                      
                    </tr>
                  );
                })}
            </tbody>
          </table>

          <Pagination
            totalPages={totalPages}
            currentPage={page}
            handlePrevPage={handlePrevPage}
            handleNextPage={handleNextPage}
          />
        </div>
      </div>
    </>
  );
};

export default Table;
