export const titleList = {
  heading: "Ứng dụng đang được kết nối. Xin chờ trong giây lát...",
};
// eslint-disable-next-line
export const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;

export const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAPS as string;

export interface MapProps {
  center: {
    lat: number;
    lng: number;
  };
  zoom: number;
  address: string;
}

export interface DataProps {
  link: string;
  key: string;
  mesage: string;
  cdrID: string;
  historyID: string;
}

export interface PermisionProps {
  location: boolean;
  zoom: boolean;
  take_photo: boolean;
  statusphoto: boolean;
}
