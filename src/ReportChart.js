import Footer from "./components/Footer";
import Header from "./components/Header";
import Chart from "./Chart";
const Employee = () => {
  return (
    <>
      <Header />
      <div>
        <Chart />
      </div>
      {<Footer />}
    </>
  );
};

export default Employee;
