import React, { useEffect, useState,useRef } from "react";
import ZoomMtgEmbedded from "@zoomus/websdk/embedded";
import axios from "axios";
import "./index.css";
import "./scss/index.scss";
//import { ZoomMtg } from "@zoomus/websdk";
import GoogleMapReact from "google-map-react";
import marker from "./assets/images/marker.png";
import TableImg from "./TableImg";
import Spinner from "./Spinner";

const titleList = {
  heading: "Ứng dụng đang được kết nối. Xin chờ trong giây lát...",
};

const Marker = (props: any) => {
  return (
    <div className="SuperAwesomePin">
      <img src={marker} alt="" />
    </div>
  );
};

interface IZoomProps {}
function Zoom(props: IZoomProps) {
  const [loading, setLoading] = useState<boolean>(false);
  const [sig, setSig] = useState<string>("");
  const [checkJoin, setCheckJoin] = useState<boolean>(false);
  const [key, setKey] = useState<{
    sdkey: string;
    secret: string;
    api_key: string;
    api_sec: string;
    meetingNumber: string;
    password: string;
    username: string;
  }>({
    sdkey: "",
    secret: "",
    api_key: "",
    api_sec: "",
    meetingNumber: "",
    password: "",
    username: "",
  });
  let token = localStorage.getItem("access_token");
  let axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  };

  const client = ZoomMtgEmbedded.createClient();

  // useEffect(() => {
  //    ZoomMtg.setZoomJSLib('https://source.zoom.us/2.5.0/lib', '/av');

  //   // ZoomMtg.preLoadWasm();
  //   // ZoomMtg.prepareWebSDK();
  //   // loads language files, also passes any error messages to the ui
  //   ZoomMtg.i18n.load("vi-VN");
  //   ZoomMtg.i18n.reload("vi-VN");
  // }, []);
  // setup your signature endpoint here: https://github.com/zoom/meetingsdk-sample-signature-node.js
  var signatureEndpoint = `${process.env.REACT_APP_API_URL}staff-api/create`;
  var userEmail = "";
  //TODO: get info to api-login
  // pass in the registrant's token if your meeting or webinar requires registration. More info here:
  // Meetings: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/meetings#join-registered
  // Webinars: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/webinars#join-registered
  var registrantToken = "";

  const onCloseMeeting = (e: any) => {
    e?.path?.forEach((element: any) => {
      if (
        // element.textContent === "Rời khỏi cuộc họp" ||
        element.textContent === "Kết thúc cuộc họp cho tất cả"
      ) {
        window.location.reload();
        return;
      }
    });
  };

  useEffect(() => {
    window.addEventListener("mousedown", onCloseMeeting);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const getSignature = () => {
      // e.preventDefault();
      fetch(signatureEndpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      })
        .then(async (res) => {
          let data = await res.json();
          if (res.status === 200) {
            setKey({
              sdkey: data.data.sdkey,
              secret: data.data.secret,
              api_key: data.data.api_key,
              api_sec: data.data.api_sec,
              username: data.data.username,
              meetingNumber: data.data.meetingNumber,
              password: data.data.password,
            });
            setSig(data.signature);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    };

    getSignature();

    // eslint-disable-next-line
  }, [token]);

  function getAudioAuto(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Âm thanh") {
        element.click();
      }
    });
  }

  function getLeadOpen(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Người tham gia") {
        element.click();
        getLeadAuto();
      }
    });
  }

  function getLeadAuto(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("span.zmwebsdk-MuiButton-label")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.textContent === "Nhận lại vị trí người chủ trì") {
        let parent = element?.closest(
          "button.zmwebsdk-MuiButtonBase-root"
        ) as HTMLElement;
        parent.click();
      }
    });
  }

  function getAutoClose(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll(
        '[role="dialog"] [class*="zmwebsdk-makeStyles-closeButton-"]'
      )
    );
    arrayBtn?.forEach((element: any) => {
      element.click();
    });
  }

  function getSecurityDisabled(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Bảo mật") {
        element.style.display = "none";
      }
      if (element.title === "Chia sẻ màn hình") {
        element.style.display = "none";
      }
    });
  }

  function handleCloseMeeting(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Rời khỏi") {
        element.addEventListener("click", function () {
          client.endMeeting();
          window.location.reload();
        });
      }
    });
  }


  function startMeeting(signature: any) {
    let meetingSDKElement = document.getElementById(
      "meetingSDKElement"
    ) as HTMLElement;

    client.init({
      debug: false,
      zoomAppRoot: meetingSDKElement,
      language: "vi-VN",
      customize: {
        video: {
          isResizable: true,
          viewSizes: {
            default: {
              width: 750,
              height: 400,
            }
          },
        },
        meetingInfo: [
          "topic",
          "host",
          "mn",
          "pwd",
          "telPwd",
          "invite",
          "participant",
          "dc",
          "enctype",
        ],
        // toolbar: {
        //   buttons: [
        //     {
        //       text: "Record",
        //       className: "CustomleaveBtn",
        //       onClick: () => {
                
        //         client.record('start')
        //           .then((e) => {
        //             console.log('record success')
        //           })
        //           .catch((e) => {
        //             console.log('levaeeee catch' , e);
        //           });
             
        //        },
        //     },
        //   ],
        // },
      },
    });

    client
      .join({
        signature: signature,
        meetingNumber: key.meetingNumber,
        userName: "MCREDIT",
        sdkKey: key.sdkey,
        userEmail: userEmail,
        password: key.password,
        tk: registrantToken,
      })
      .then((e) => {
        setLoading(false);
        getSecurityDisabled();
        getLeadOpen();
        getAudioAuto();
        getAutoClose();
        handleCloseMeeting()
        var getAttendeeslist = client.getCurrentMeetingInfo();
        //console.log(getAttendeeslist)
        document.title = getAttendeeslist.meetingTopic;
        var getAll = client.getAttendeeslist()
        console.log(getAll)
      })
      .catch((e) => {
        //console.log("join error", e);
      });
  }

  const handleClickJoin = () => {
    startMeeting(sig);
    setCheckJoin(true);
    setLoading(true);
  };

  const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAPS as string;

  interface MapProps {
    center: {
      lat: number;
      lng: number;
    };
    zoom: number;
    address:string;
  }

  const initialMapProps: MapProps = {
    center: {
      lat: 0,
      lng: 0,
    },
    zoom: 15,
    address:''
  };

  const [data, setData] = useState<any>("");
  const [keyCustomer, setKeyCustomer] = useState<string>("");
  const [phone, setPhone] = useState<string>("");

  const [errorText, setErrorText] = useState<string>("");
  const [valid, setValid] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);
  const [activeMsg, setActiveMsg] = useState<boolean>(false);
  const [mapActive, setMapActive] = useState<boolean>(false);
  const [openMap, setOpenMap] = useState<boolean>(false);

  const [dataImg, setDataImg] = useState<any>("");
  const [showImg, setShowImg] = useState<boolean>(false);

  const [mapProps, setMapProps] = useState<MapProps>(initialMapProps);

  const [initPhone, setInitPhone] = useState<string>("");

  const [phoneSuccess, setPhoneSuccess] = useState<boolean>(true);

  // eslint-disable-next-line no-useless-escape
  const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;

  const validatePassword = (value: string) => {
    if (!phoneRegExp.test(value)) {
      setValid(false);
    } else {
      setValid(true);
    }
  };

  function onChange(e: any) {
    setPhone(e.target.value);
    validatePassword(e.target.value);
  }

  const handleCreate = async () => {
    const bodyParameters = {
      phone: phone,
    };
    axios
      .post(
        `${process.env.REACT_APP_API_URL}staff-api/send-sms`,
        bodyParameters,
        axiosConfig
      )
      .then((res) => {
        if (key.meetingNumber && res.status === 200) {
          setInitPhone(phone);
          setActiveMsg(true);
          setData(res.data.link);
          setKeyCustomer(res.data.key);
          setMapActive(true);
          setShowError(false);
          setPhoneSuccess(false);
        } else {
          setErrorText("Tài khoản nhân viên không tồn tại");
          setShowError(true);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  //sau do goi ham get de lay thong tin lat long sau khi update bên phía client

  const getLocationCustomer = async () => {
    axios
      .get(`${process.env.REACT_APP_API_URL}staff-api/address/${keyCustomer}`)
      .then((res) => {
        if (
          res.status === 200 &&
          (res.data.longitude !== -200 || res.data.latitude !== -200)
        ) {
          const nextMapProps = {
            center: {
              lat: res.data.latitude,
              lng: res.data.longitude,
            },
            zoom: 15,
            address:res.data.address
          };
          setMapProps(nextMapProps);
          setShowError(false);
        } else {
          setErrorText("Khách hàng không chia sẻ vị trí");
          setShowError(true);
        }
      });
  };

  //get images

  const handleGetAttachments = async () => {
    axios
      .get(`${process.env.REACT_APP_API_URL}api/upload/${keyCustomer}`)
      .then((res) => {
        if (res.status === 200) {
          setDataImg(res.data.data.img_list);
          setShowImg(true);
        } else {
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  //copy clipboard

  const copyClipboard = async () => {
    await navigator.clipboard.writeText(data);
  };




//close metting by close tab browser

  useEffect(() => {
    window.addEventListener("beforeunload", function (event) {
      client.endMeeting();
      event.preventDefault();
      
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  //websocket

  interface PermisionProps {
    location:boolean,
    zoom:boolean,
    status_photo:boolean,
  }

  const initialPermisionProps: PermisionProps = {
    location:false,
    zoom:false,
    status_photo:false,
  };

  const [msgSocket, setMsgSocket] = useState<any>([]);
  const [permision, setPermision] = useState<PermisionProps>(initialPermisionProps);
  const [showErrorTakePhoto, setShowErrorTakePhoto] = useState<boolean>(false);
  const [showMsgStatus, setShowMsgStatus] = useState<boolean>(false);

  const ws = useRef<any>(null);

  
  useEffect(() => {
   
    if(keyCustomer){
      let url = `wss:graph.siptrunk.vn/ws/chat/${keyCustomer}/`;
      ws.current = new WebSocket(url);
        ws.current.onmessage = (e:any) => {
          const message = JSON.parse(e.data);
          setMsgSocket((prev:any) => [...prev, message]);
      };
    }
    
       
  }, [keyCustomer])
  
  useEffect(() => {
   if(msgSocket){
      let checkPermisionLoc = msgSocket.some((item:any) => item.message === 'Share Geolocation')
      let checkPermisionZoom = msgSocket.some((item:any) => item.message === 'Accept Zoom')
      let checkStatus = msgSocket.some((item:any) => item.message === 'Take Photo Success')
      const nextProps  = { 
        location:checkPermisionLoc,
        zoom:checkPermisionZoom,
        status_photo:checkStatus
      }
      setPermision(nextProps)

   }
    
  }, [msgSocket])


  const handleTakePhotoByStaff = () => {
    
    if(permision.zoom === true){
      ws.current.send(JSON.stringify({
        'message': 'Take Photo'
    }));
    } else{
      setShowErrorTakePhoto(true)
    }
  }

  useEffect(() => {
    if(permision.status_photo === true){
      
      setTimeout(() => {
        setShowMsgStatus(true)
      }, 2000);
      
   }
  }, [permision])
  console.log(msgSocket)

  return (
    <>
      {!checkJoin && (
        <div className="video-form text-center">
          <h3 className="title">Cuộc gọi thẩm định </h3>
          <button className="btn-startcall" onClick={() => handleClickJoin()}>
            Bắt đầu
          </button>
        </div>
      )}
      {loading ? <Spinner heading={titleList.heading} /> : null}
      <div className="container container-video">
        <div className="credit-content">
          {checkJoin && (
            <>
              <div className="form-inline form-create-call">
                <h3
                  className="title"
                  style={{ marginBottom: 10, fontSize: "20px" }}
                >
                  Thông tin khách hàng
                </h3>
                {showError && (
                  <div className="alert alert-danger" role="alert">
                    {errorText}
                  </div>
                )}
                {
                  showErrorTakePhoto && (
                    <div className="alert alert-danger" role="alert">
                      'Khách hàng chưa cấp quyền truy cập ảnh'
                  </div>
                  )}

                {
                  permision.status_photo === true && showMsgStatus === false && (
                    <div className="alert alert-danger" role="alert">
                      Chụp thất bại 
                  </div>
                  )
                }
                {
                  permision.status_photo === true && showMsgStatus === true && (
                    <div className="alert alert-success" role="alert">
                      Chụp thất bại 
                  </div>
                  )
                }
                {phoneSuccess && (
                  <>
                    <div className="form-input mb-2">
                      <label style={{ fontSize: 14, marginRight: 15 }}>
                        Số điện thoại
                      </label>
                      <input
                        style={{ width: "170px" }}
                        type="text"
                        className={` form-control ${valid ? "is-valid" : ""}`}
                        name="phone"
                        value={phone}
                        onChange={onChange}
                      />
                    </div>

                    <button
                      style={{ display: "block" }}
                      disabled={valid ? false : true}
                      className="btn btn-primary mb-2"
                      type="submit"
                      onClick={() => handleCreate()}
                    >
                      Gửi tin nhắn
                    </button>
                  </>
                )}

                {mapActive && (
                  <div className="text-left">
                    <button
                      className="btn btn-warning"
                      id="button-capture-id-extension"
                      style={{ marginRight: 5 }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-camera"
                      ></i>
                      Chụp ảnh
                    </button>
                    <button
                      style={{ marginRight: 5 }}
                      className="btn btn-warning"
                      onClick={() => {
                        handleGetAttachments();
                        setOpenMap(false);
                      }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-file-image-o"
                      ></i>
                      Tệp ảnh
                    </button>

                    <button
                      style={{ marginRight: 5, marginTop: 5 }}
                      className="btn btn-warning"
                      onClick={() => {
                        setOpenMap(true);
                        setShowImg(false);
                        getLocationCustomer();
                      }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-map-marker"
                      ></i>
                      Xem vị trí
                    </button>

                    <button
                      className="btn btn-warning"
                      disabled={permision.zoom === false}
                      style={{ marginRight: 5, marginTop: 5 }}
                      onClick={()=>handleTakePhotoByStaff()}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-camera"
                      ></i>
                      Chụp ảnh KH
                    </button>
                  </div>
                )}
                {
                 permision.zoom === true && (
                    <div style={{marginTop:10,fontSize:14,fontWeight:600,fontStyle:'italic'}}>
                      *Chú ý: Nhấn nút chụp lần nữa để chụp ảnh khách hàng.
                    </div>
                  )
                }
                {showImg && <TableImg dataImg={dataImg} />}
                {
                  openMap && (
                    <div style={{marginTop:10,fontSize:14,fontWeight:600}}>
                      Địa chỉ: {mapProps.address}
                    </div>
                  )
                }
                    
                <div
                  style={{
                    opacity: `${openMap ? 1 : 0}`,
                    height: `${openMap ? "300px" : "inherit"}`,
                    width: "100%",
                    marginTop: 10,
                  }}
                >
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: googleMapsApiKey }}
                    center={mapProps.center}
                    defaultZoom={mapProps.zoom}
                  >
                    <Marker
                      lat={mapProps.center.lat}
                      lng={mapProps.center.lng}
                    />
                  </GoogleMapReact>
                </div>
              </div>

              <div className={`card-body ${activeMsg ? "active" : ""}`}>
                <h5
                  className="card-title"
                  style={{ fontSize: 24, marginBottom: 10 }}
                >
                  Thông tin khách hàng
                </h5>
                <p className="card-text">
                  {initPhone} gửi sms{" "}
                  <span className="text-success fw-bold">Sucesss</span> Thông
                  tin đường dẫn gửi khách hàng nếu sms không đến
                </p>
                <div className="form-input">
                  <button onClick={copyClipboard} disabled={!data}>
                    <i className="fa fa-paperclip"></i>
                  </button>
                  <input
                    type="text"
                    className="form-control"
                    value={data}
                  ></input>
                </div>
              </div>
            </>
          )}
        </div>

        {/* For Component View */}
        <div className="videocall">
          <div
            id="meetingSDKElement"
            style={{
              justifyContent: "center",
              marginTop: "30px",
              display: "flex",
            }}
          ></div>
          {/* Zoom Meeting SDK Component View Rendered Here */}
        </div>
      </div>
    </>
  );
}

export default Zoom;
