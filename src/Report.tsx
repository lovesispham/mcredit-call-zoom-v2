import Footer from "./components/Footer";
import Header from "./components/Header";
import TableReport from "./Table";
const Employee = () => {
  return (
    <>
      <Header />
      <div>
        <TableReport />
      </div>
      {<Footer />}
    </>
  );
};

export default Employee;
