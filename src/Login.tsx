import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
const getElmById = (elmId: string) => {
  return document.getElementById(elmId) as HTMLElement;
};
const Login = () => {
  const history = useHistory();

  const [isAdmin, setIsAdmin] = React.useState(null);
  const [showMsg, setShowMsg] = React.useState<boolean>(false);
  const [msgText, setMsgText] = React.useState<string>("");
  const [valueInput, setValueInput] = React.useState<{
    username: string;
    password: string;
  }>({
    username: "",
    password: "",
  });

  const handleChangeInput = (e: any) => {
    const { name, value } = e.target;
    setValueInput({ ...valueInput, [name]: value });
  };

  const submitForm = async () => {
    const bodyParameters = {
      username: valueInput.username,
      password: valueInput.password,
    };
    axios
      .post(`${process.env.REACT_APP_API_URL}api/mytoken/`, bodyParameters)
      .then((res) => {
        const myInfo = {
          username:res.data.username,
          isAdmin: res.data.is_superuser
        }
        localStorage.setItem("access_token", res.data.token);
        localStorage.setItem("myinfo", JSON.stringify(myInfo));
        setIsAdmin(res.data.is_superuser);
      })
      .catch((error) => {
        setShowMsg(true);
        setMsgText("Sai tên đăng nhập hoặc mật khẩu");
      });
  };

  useEffect(() => {
    if (isAdmin === true) {
      history.push("report");
    }
    if (isAdmin === false) {
      history.push("staff");
    }
    // eslint-disable-next-line
  }, [isAdmin]);

  const detectEnter = (event: any) => {
    if (event.keyCode === 13) {
      event.preventDefault();
      getElmById("loginBtn").click();
    }
  };

  return (
    <div className="login-form">
      <h3 className="title text-center">Đăng nhập</h3>
      {showMsg && (
        <div
          style={{ padding: 10, fontSize: 14 }}
          className="alert alert-danger"
          role="alert"
        >
          {msgText}
        </div>
      )}
      <div className="form-input">
        <label className="label-name">Tên đăng nhập</label>
        <input
          onKeyUp={detectEnter}
          onChange={handleChangeInput}
          id="username"
          type="text"
          className="form-control"
          name="username"
          placeholder="Tên đăng nhập"
          value={valueInput.username}
        />
      </div>
      <div className="form-input">
        <label className="label-name">Mật khẩu</label>
        <input
          onKeyUp={detectEnter}
          onChange={handleChangeInput}
          type="password"
          className="form-control"
          name="password"
          placeholder="Nhập mật khẩu"
          value={valueInput.password}
        />
      </div>
      <div className="form-input text-right">
        <button
          id="loginBtn"
          className="btn bg-primary"
          type="submit"
          onClick={() => submitForm()}
        >
          Đăng nhập
        </button>
      </div>
    </div>
  );
};

export default Login;
