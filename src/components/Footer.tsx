import React from "react";

import "../scss/index.scss";

const Footer = () => {
  return (
    <div className="footer-mcall">
      <div className="copyright">
        <div className="container">
          <p>
            © 2016 Bản quyền thuộc về Công ty Tài chính TNHH MB Shinsei
            (Mcredit)
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
