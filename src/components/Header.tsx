import React, { useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import logo from "../assets/images/logo.jpg";
import "../scss/index.scss";
import ZoomMtgEmbedded from "@zoomus/websdk/embedded";
const mainMenu = [
  {
    display: "Báo cáo",
    path: "/report",
  },
  {
    display: "Thống kê",
    path: "/report-table",
  },
];

const Header = () => {
  const menuLeft = useRef(null);
  const [menuOpen, setMenuOpen] = useState(false);


  const client = ZoomMtgEmbedded.createClient();
  const location = useLocation();
  const url = location.pathname
  useEffect(() => {
    if(url !== '/staff' || '/staff3'){
      client.endMeeting();
      ZoomMtgEmbedded.destroyClient()
    }
    // eslint-disable-next-line
  }, [url])

  const handleLogOut = () => {
    localStorage.removeItem("access_token"); //item
    localStorage.removeItem("username"); //item
    localStorage.removeItem("myinfo"); //item
    window.location.reload();
  };
  const myInfo = JSON.parse(String(localStorage.getItem('myinfo')));

  const pathname = useLocation();

  const activeNav = mainMenu.findIndex(
    (e: any) => e.path === pathname.pathname
  );
  return (
    <div className="header credit">
      <div className="navbar-bottom">
        <div className="container">
          <div className="logo">
            <a href="/" className="logo">
              <img src={logo} alt="" />
            </a>
          </div>
          <div className={`mobile-nav ${menuOpen ? 'active' : ''}`} ref={menuLeft}>
            <ul className="navbar-nav">
              {mainMenu.map((item, index) => (
                <li
                  className={`level1 ${index === activeNav ? "active" : ""}`}
                  key={index}
                >
                  <Link to={item.path}>{item.display}</Link>
                </li>
              ))}
            </ul>
          </div>
          <div className="header-user-info">
            <div className="photo">{myInfo.username[0]}</div>
            <div className="username">
              <span className="role-name">{myInfo.isAdmin === true ? 'ADMIN' : 'USER'}</span>
              {myInfo.username}
            </div>
            <ul className="box-user">
              <li>
                <Link to="/info">Đổi mật khẩu</Link>
              </li>
              <li>
                <Link to="/" onClick={() => handleLogOut()}>
                  Đăng xuất
                </Link>
              </li>
            </ul>
          </div>
          <span className="icon-pushmenu navbar-toggle" onClick={()=> setMenuOpen(!menuOpen)}>
          <i className="fa fa-bars"></i>
        </span>
        </div>
      </div>
    </div>
  );
};

export default Header;
