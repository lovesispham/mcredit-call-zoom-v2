import React, { useEffect, useState, useRef } from "react";
import ZoomMtgEmbedded from "@zoomus/websdk/embedded";
import axios from "axios";
import "./index.css";
import "./scss/index.scss";
import GoogleMapReact from "google-map-react";
import marker from "./assets/images/marker.png";
import TableImg from "./TableImg";
import Spinner from "./Spinner";
import Feedback from "./Feedback";

import {
  titleList,
  googleMapsApiKey,
  MapProps,
  DataProps,
  PermisionProps,
  

} from "./constants";
import urlApi from "./urlApi";


import Toast from "./Toast";
import { useLocation } from "react-router-dom";
import img from "./assets/images/call-verify.jpeg";
import { useWindowSize } from "./useWindowSize";

const Marker = (props: any) => {
  return (
    <div className="SuperAwesomePin">
      <img src={marker} alt="" />
    </div>
  );
};

type IToken =  {message: string, show: boolean}
 
type ILoading = {
  attachment:boolean,
  location:boolean
}

function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

interface IZoomProps {}
function Zoom(props: IZoomProps) {
  let query = useQuery();
  const test = query.get("test");
  const username = query.get("username");
  const phone = query.get("phone");
  const code = query.get("code");
  const type = query.get("type");
  const isType1 = type && ['sms','app'].includes(type)
  const isType2 = type && ['afs'].includes(type)
  const isTypeAccept = type && ['sms','app','afs'].includes(type)

  const [loading, setLoading] = useState<boolean>(false);
  const [loadingBtn, setLoadingBtn] = useState<ILoading>({
    attachment:false,
    location:false,
  });
  const [sig, setSig] = useState<string>("");
  const [token, setToken] = useState<string>("");
  const [messageApiToken, setMessageApiToken] = useState<IToken>();
  

  const initialMapProps: MapProps = {
    center: {
      lat: 0,
      lng: 0,
    },
    zoom: 15,
    address: "",
  };

  const initialDataProps: DataProps = {
    link: "",
    key: "",
    mesage: "",
    cdrID: "",
    historyID: "",
  };

  const [data, setData] = useState<DataProps>(initialDataProps);

  const [record, setRecord] = useState<boolean>(true);
  const [activeRecord, setActiveRecord] = useState(true);

  const [errorText, setErrorText] = useState<string>("");
  
  const [showError, setShowError] = useState<boolean>(false);


  const [mapActive, setMapActive] = useState<boolean>(false);
  
  const [openMap, setOpenMap] = useState<boolean>(false);
  const [checkCodeMc, setCheckCodeMc] = useState<boolean>(false);
  const [codeMc,setCodeMc] = useState<any>("");
  const [dataImg, setDataImg] = useState<any>("");
  const [showImg, setShowImg] = useState<boolean>(false);

  const [mapProps, setMapProps] = useState<MapProps>(initialMapProps);


    //websocket

    const initialPermisionProps: PermisionProps = {
      location: false,
      zoom: false,
      take_photo: false,
      statusphoto: false,
    };
  
    const [msgSocket, setMsgSocket] = useState<any>([]);
    const [permision, setPermision] = useState<PermisionProps>(
      initialPermisionProps
    ); // eslint-disable-next-line
    const [showErrorTakePhoto, setShowErrorTakePhoto] = useState<boolean>(false);
    const [text, setText] = useState<string>("Chụp ảnh KH");
  
    const ws = useRef<any>(null);

  const [key, setKey] = useState<{
    sdkey: string;
    secret: string;
    api_key: string;
    api_sec: string;
    meetingNumber: string;
    password: string;
    username: string;
  }>({
    sdkey: "",
    secret: "",
    api_key: "",
    api_sec: "",
    meetingNumber: "",
    password: "",
    username: "",
  });
  const client = ZoomMtgEmbedded.createClient();

  // setup your signature endpoint here: https://github.com/zoom/meetingsdk-sample-signature-node.js
  var signatureEndpoint = `${process.env.REACT_APP_API_URL}staff-api/create`;
  var userEmail = "";
  //TODO: get info to api-login
  // pass in the registrant's token if your meeting or webinar requires registration. More info here:
  // Meetings: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/meetings#join-registered
  // Webinars: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/webinars#join-registered
  var registrantToken = "";

  // auto click function when join call
  const onCloseMeeting = (e: any) => {
    e?.path?.forEach((element: any) => {
      if (
        // element.textContent === "Rời khỏi cuộc họp" ||
        element.textContent === "Kết thúc cuộc họp cho tất cả"
      ) {
        setModalFeed(true);
        //window.location.reload();
        return;
      }
    });
  };

  useEffect(() => {
    window.addEventListener("mousedown", onCloseMeeting);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (username) {
      const getToken = () => {
        const body = {
          username: username,
        };
        axios
          .get(urlApi.getToken, { params: body })
          .then((response: any) => {
            const myInfo = {
              username: username,
              isAdmin: response.data.is_superuser,
            };
            localStorage.setItem("access_token", response.data.token);
            localStorage.setItem("myinfo", JSON.stringify(myInfo));
            setToken(response.data.token);
            
          })
          .catch((err:any) => {
            
            setMessageApiToken({
              show:true,
              message: err?.response?.data?.message
            })
            
          });
      };
      getToken();
    }
  }, [username]);

  


  let axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  };

  useEffect(() => {
    if (token && isTypeAccept) {
      const getSignature = () => {
        // e.preventDefault();
        fetch(signatureEndpoint, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Token ${token}`,
          },
        })
          .then(async (res) => {
            let data = await res.json();
            if (res.status === 200) {
              setKey({
                sdkey: data.data.sdkey,
                secret: data.data.secret,
                api_key: data.data.api_key,
                api_sec: data.data.api_sec,
                username: data.data.username,
                meetingNumber: data.data.meetingNumber,
                password: data.data.password,
              });
              setSig(data.signature);
            }
          })
          .catch((error) => {
            console.error(error);
          });
      };

      getSignature();
    }

    // eslint-disable-next-line
  }, [token ,isTypeAccept]);
// autoclose zoom
  function getAudioAuto(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Âm thanh") {
        element.click();
      }
    });
  }

  function handleAutoCamera(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Bắt đầu video") {
        element.click();
      }
    });
  }

  function getLeadOpen(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Người tham gia") {
        element.click();
        getLeadAuto();
      }
    });
  }

  function getLeadAuto(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("span.zmwebsdk-MuiButton-label")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.textContent === "Nhận lại vị trí người chủ trì") {
        let parent = element?.closest(
          "button.zmwebsdk-MuiButtonBase-root"
        ) as HTMLElement;
        parent.click();
      }
    });
  }

  function getAutoClose(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll(
        '[role="dialog"] [class*="zmwebsdk-makeStyles-closeButton-"]'
      )
    );
    arrayBtn?.forEach((element: any) => {
      element.click();
    });
  }

  function getSecurityDisabled(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Bảo mật") {
        element.style.display = "none";
      }
      if (element.title === "Chia sẻ màn hình") {
        element.style.display = "none";
      }
    });
  }

  function handleCloseMeeting(): void {
    let arrayBtn = Array.from(
      document.querySelectorAll("button.zmwebsdk-MuiButtonBase-root")
    );
    arrayBtn?.forEach((element: any) => {
      if (element.title === "Rời khỏi") {
        element.addEventListener("click", function () {
          //client.endMeeting();
          setModalFeed(true);
          //window.location.reload();
        });
      }
    });
  }

  var getAll = client.getAttendeeslist();
  var getRoom = client.getCurrentMeetingInfo();
  var checkTotalJoin = getAll.length;
  var checkRecord = getAll.filter(
    (item) => item.isHost === false && item.displayName !== "PTF Việt Nam"
  );
  var checkCustomerJoin = getAll.filter(
    (item) => item.isHost === false 
  ) as any


useEffect(() => {

  if(getAll){
    let userHost = getAll.filter(v => v.isHost === false && v.displayName ===  `${process.env.REACT_APP_WEBSITE}` )
    if(userHost?.length > 0){
      
      userHost.map(v => {
        
          client.expel(v?.userId);
          client.endMeeting();
          ZoomMtgEmbedded.destroyClient();
        
      })
    }
    
  }
  // eslint-disable-next-line
}, [getAll])

useEffect(() => {
  let checkPermisionZoom = msgSocket.some(
    (item: any) => item.message === "Accept Zoom"
  );
  if(getAll){
    let userCustomer = getAll.filter(v => v.isHost === false && v.displayName ===  phone )
    if(checkPermisionZoom && userCustomer.length > 0){
      const newarr = userCustomer.splice(1)
      newarr?.map(v => {
        
        return client.expel(v?.userId);
       
      
    })
   }
  }
   
  
   // eslint-disable-next-line
}, [getAll, msgSocket])
  useEffect(() => {
   
    if (checkRecord.length === 1) {
      // handleClickAdd()
      client.record("start");
      updateActiveRecord();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checkTotalJoin]);
  const size = useWindowSize();

  



  
  function startMeeting(signature: any) {
    let meetingSDKElement = document.getElementById(
      "meetingSDKElement"
    ) as HTMLElement;

    client.init({
      debug: false,
      zoomAppRoot: meetingSDKElement,
      language: "vi-VN",
      customize: {
        video: {
          isResizable: true,
          viewSizes: {
            default: {
              width: size.width > 812 ? (size.width/2) : size.width,
              height: 400,
            },
          },
        },
        meetingInfo: [
          "topic",
          "host",
          "mn",
          "pwd",
          "telPwd",
          "invite",
          "participant",
          "dc",
          "enctype",
        ],
      },
    });

    client
      .join({
        signature: sig,
        meetingNumber: key.meetingNumber,
        userName: `${process.env.REACT_APP_WEBSITE}`,
        sdkKey: key.sdkey,
        userEmail: userEmail,
        password: key.password,
        tk: registrantToken,
      })
      .then((e) => {
        setLoading(false);
        getSecurityDisabled();
        getLeadOpen();
        getAudioAuto();
        handleAutoCamera();
        getAutoClose();
        handleCloseMeeting();
        var getAttendeeslist = client.getCurrentMeetingInfo();
        //console.log(getAttendeeslist)
        document.title = getAttendeeslist.meetingTopic;
        // var getAll = client.getAttendeeslist()
        // console.log(getAll)
      })
      .catch((e) => {
        console.log("join error", e);
      });
  }

  useEffect(() => {
    if (sig && key && username) {
      if(checkCodeMc === true) {
        startMeeting(sig);
        setLoading(true);
      } else {
        setLoading(true);
      }
      
    }
    // eslint-disable-next-line
  }, [sig, key, username, checkCodeMc]);


  
  const checkType = (typeText:string) =>{
    if (typeText === 'sms'){
      return '0'
    }
    if (typeText === 'app'){
      return '1'
    }
    
  }



  useEffect(() => {
    if (loading === true && phone && isType1 && code) {
      const handleCreate = async () => {
        const bodyParameters = {
          phone: phone,
          code: code,
          type: type && checkType(type)
        };
        axios
          .post(
            `${process.env.REACT_APP_API_URL}staff-api/send-sms`,
            bodyParameters,
            axiosConfig
          )
    
          .then((res) => {
            if (key.meetingNumber && res.status === 200) {
              setTimeout(() => {
                setData(res.data); //
    
                setMapActive(true);
              }, 2200);
            } else {
              setMessageApiToken(
                {
                  message:'Tài khoản nhân viên không tồn tại',
                  show:true,
                }
              )
            }
          })
          .catch((error) => {
            console.error(error);
          });
      };
      handleCreate();
    }
  }, [loading, phone, code , isType1]);


  useEffect(() => {
    if (loading === true && phone && isType2 && code) {
      const handleCreate = async () => {
        const urlByTest = (test !== 'default' ? urlApi.sendMcNoti : urlApi.sendMcNotiTest)
        const body = {
          phone: phone,
        }
        axios
          .get(
            urlByTest,
            {
              params:body,
              headers: {
                "Content-Type": "application/json",
                Authorization: `Token ${token}`,
              },
            }
          )
    
          .then((res) => {
            
            if(res?.data?.code === 500){
              setCheckCodeMc(false)
              setLoading(false)
              setCodeMc(JSON.parse(res?.data?.data))
            } else {
              setMapActive(true);
              setCheckCodeMc(true)
            }
            
          })
          .catch((error) => {
            
          });
      };
      handleCreate();
    }
  }, [loading, phone, code , isType2, test]);

  const updateInforRecord = () => {
    const bodyParameters = {
      cdrID: data?.cdrID,
      historyID: data?.historyID,
      uuid: getRoom.meetingId,
    };
    axios
      .post(
        `${process.env.REACT_APP_API_URL}api/update-cdr?`,
        bodyParameters,
        axiosConfig
      )

      .then((res) => {
        // console.log(res)
        if (res.status === 200) {
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const updateActiveRecord = () => {
    const bodyParameters = {
      uuid: getRoom.meetingId,
    };
    axios
      .post(
        `${process.env.REACT_APP_API_URL}api/active-record/`,
        bodyParameters,
        axiosConfig
      )

      .then((res) => {
        if (res.status === 200) {
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    if (
      getRoom.meetingId !== "" &&
      data.cdrID !== "" &&
      data.historyID !== ""
    ) {
      updateInforRecord();
    }
    // eslint-disable-next-line
  }, [data.cdrID, data.historyID]);

  //sau do goi ham get de lay thong tin lat long sau khi update bên phía client

  const getLocationCustomer = async () => {
    setLoadingBtn({
      attachment:false,
      location:true
    })
    axios
      .get(`${process.env.REACT_APP_API_URL}staff-api/address/${data.key}`)
      .then((res) => {
        setLoadingBtn({
          attachment:false,
          location:true
        })
        if (
          res.status === 200 &&
          (res.data.longitude !== -200 || res.data.latitude !== -200)
        ) {
          const nextMapProps = {
            center: {
              lat: res.data.latitude,
              lng: res.data.longitude,
            },
            zoom: 15,
            address: res.data.address,
          };
          setMapProps(nextMapProps);
          
        } else {
          setLoadingBtn({
            attachment:false,
            location:false
          })
          setMessageApiToken({
            message:'Khách hàng không chia sẻ vị trí',
            show:true,
          })
          
        }
      }).catch(err =>{
        setLoadingBtn({
          attachment:false,
          location:false
        })
      })
  };

  //get images

  const handleGetAttachments = async () => {
    setLoadingBtn({
      attachment:true,
      location:false
    })
    axios
      .get(`${process.env.REACT_APP_API_URL}api/upload/${data.key}`)
      .then((res) => {
        setLoadingBtn({
          attachment:false,
          location:false
        })
        if (res.status === 200) {
          setDataImg(res.data.data.img_list);
        } else {
        }
      })
      .catch((error) => {
        setLoadingBtn({
          attachment:false,
          location:false
        })
      });
  };




  //close metting by close tab browser

  useEffect(() => {
    window.addEventListener("beforeunload", function (event) {
      client.endMeeting();
      ZoomMtgEmbedded.destroyClient();
      event.preventDefault();
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


      // //ip
      // localStorage.openpages = Date.now();
      // var onLocalStorageEvent = function(e:any){
      //   if(e.key === "openpages"){
      //     // Emit that you're already available.
      //     localStorage.page_available = Date.now();
      //   }
      //   if(e.key === "page_available"){
      //     alert('Cuộc gọi thẩm định đã bắt đầu. Xin hãy trở lại cửa sổ trình duyệt ban đầu.')
      //   }
      // };
      // window.addEventListener('storage', onLocalStorageEvent, false);





  useEffect(() => {
    if (data.key) {
      let url = `wss:mvideocall.cf/ws/chat/${data.key}/`;
      ws.current = new WebSocket(url);
      ws.current.onmessage = (e: any) => {
        const message = JSON.parse(e.data);
        setMsgSocket((prev: any) => [...prev, message]);
      };
    }
  }, [data.key]);


  useEffect(() => {
    if (msgSocket) {
    
      
      let checkPermisionLoc = msgSocket.some(
        (item: any) => item.message === "Share Geolocation"
      );
      let checkPermisionZoom = msgSocket.some(
        (item: any) => item.message === "Accept Zoom"
      );
      let takePhoto = msgSocket.some((item: any) => item.message === "again");
      let status = msgSocket.some((item: any) => item.message === "success");
      const nextProps = {
        location: checkPermisionLoc,
        zoom: checkPermisionZoom,
        take_photo: takePhoto,
        statusphoto: status,
      };
      setPermision(nextProps);
      
    }
  }, [msgSocket]);




  const handleTakePhotoByStaff = () => {
    if (permision.zoom === true) {
      ws.current.send(
        JSON.stringify({
          message: "Take Photo",
        })
      );
    }
  };

  useEffect(() => {
    if (permision.take_photo === true) {
      setText("Nhấn Chụp lần nữa");
    } else {
      setText("Chụp ảnh KH");
    }
  }, [permision.take_photo]);

  useEffect(() => {
    if (permision.statusphoto === true) {
      const filter = msgSocket.filter(
        (item: any) =>
          item.message !== "again" &&
          item.message !== "success" &&
          item.message !== "Take Photo"
      );
      setMsgSocket(filter);
    }
  }, [msgSocket, permision.statusphoto]);

  const [modalFeedback, setModalFeed] = useState<boolean>(false);

  // Record
  const handleRecord = () => {
    client.record("start");
    setActiveRecord(false);
  };




  return (
    <>
      {!username || !token || !code || !phone || !type || !['sms','afs','app'].includes(type) ? (
        <>
        
        <div className="video-form text-center" style={{ boxShadow: "none" }}>

              <h3 className="title" style={{lineHeight:'44px'}}>
              Chào mừng,
              <br></br> đến với cuộc gọi thẩm định
            </h3>

          
          <img src={img} alt="security" style={{width:300}} />
        </div>

        {messageApiToken?.show && (
          <Toast
            type="error"
            message={messageApiToken?.message}
            loading={messageApiToken?.show}
            setLoading2={setMessageApiToken}
          />
        )}
        
        </>
      ) : (
        <>
          {loading ? <Spinner heading={titleList.heading} /> : null}
          {codeMc && (
          <div className="video-form text-center" style={{ boxShadow: "none" }}>
          
            
              <h3 className="title" style={{lineHeight:'44px'}}>
              {
               codeMc?.message
              }
             </h3>
            
          </div>
          )}
          <div className="container container-video">
            
            <div className="credit-content">
              {messageApiToken?.show && (
                <Toast
                  type="error"
                  message={messageApiToken?.message}
                  loading={messageApiToken?.show}
                  setLoading2={setMessageApiToken}
                />
              )}

              {modalFeedback && (
                <Feedback
                  uuidZoom={getRoom.meetingId}
                  handleEndMeeting={client.endMeeting}
                  code={code}
                />
              )}

              {mapActive && (
              <div className="form-inline form-create-call">
                
                <h3
                  className="title"
                  style={{ marginBottom: 10, fontSize: "20px" }}
                >
                  Thông tin khách hàng
                </h3>


                
                  <div className="text-left">
                    <button
                      className="btn btn-warning"
                      id="button-capture-id-extension"
                      style={{ marginRight: 5 }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-camera"
                      ></i>
                      Chụp ảnh
                    </button>
                    <button
                      style={{ marginRight: 5 }}
                      className="btn btn-warning"
                      onClick={() => {
                        handleGetAttachments();
                          setShowImg(true);
                          setOpenMap(false);
                      }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-file-image-o"
                      ></i>
                      Tệp ảnh
                    </button>

                    <button
                      style={{ marginRight: 5, marginTop: 5 }}
                      className="btn btn-warning"
                      onClick={() => {
                        setOpenMap(true);
                        setShowImg(false);
                        getLocationCustomer();
                      }}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-map-marker"
                      ></i>
                      Xem vị trí
                    </button>

                    <button
                      className="btn btn-warning"
                      disabled={permision.zoom === false}
                      style={{
                        marginRight: 5,
                        marginTop: 5,
                        background: "#D64237",
                      }}
                      onClick={() => handleTakePhotoByStaff()}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-camera"
                      ></i>
                      {text}
                    </button>
                    <button
                      className="btn btn-warning"
                      disabled={activeRecord ? false : true}
                      style={{
                        marginRight: 5,
                        marginTop: 5,
                        background: "#D64237",
                      }}
                      onClick={() => handleRecord()}
                    >
                      <i
                        style={{ marginRight: 5 }}
                        className="fa fa-video-camera"
                      ></i>
                      Record
                    </button>
                  </div>
               
                {permision.zoom === true && (
                  <div
                    style={{
                      marginTop: 10,
                      fontSize: 14,
                      fontWeight: 600,
                      fontStyle: "italic",
                    }}
                  >
                    *Chú ý: Nhấn nút chụp lần nữa để chụp ảnh khách hàng.
                  </div>
                )}
                {showImg && (
                  <TableImg setDataImg={setDataImg} dataImg={dataImg} />
                )}

                {showErrorTakePhoto && (
                  <div className="alert alert-danger" role="alert">
                    'Khách hàng chưa cấp quyền truy cập ảnh'
                  </div>
                )}

                {openMap && (
                  <>
                    {showError && (
                      <div className="alert alert-danger" role="alert">
                        {errorText}
                      </div>
                    )}
                    <div
                      style={{ marginTop: 10, fontSize: 14, fontWeight: 600 }}
                    >
                      Địa chỉ: {mapProps.address}
                    </div>
                  </>
                )}

                <div
                  style={{
                    opacity: `${openMap ? 1 : 0}`,
                    height: `${openMap ? "300px" : "inherit"}`,
                    width: "100%",
                    marginTop: 10,
                  }}
                >
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: googleMapsApiKey }}
                    center={mapProps.center}
                    defaultZoom={mapProps.zoom}
                  >
                    <Marker
                      lat={mapProps.center.lat}
                      lng={mapProps.center.lng}
                    />
                  </GoogleMapReact>
                </div>
              </div>
              )}
            </div>

            {/* For Component View */}
            <div className="videocall">
              <div
                id="meetingSDKElement"
                style={{
                  justifyContent: "center",
                  marginTop: "30px",
                  display: "flex",
                  

                }}
              ></div>
              {/* Zoom Meeting SDK Component View Rendered Here */}
            </div>
          </div>
        </>
      )}
    
    </>
  );
}

export default Zoom;
