import React from "react";
import { Autoplay, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation"; // Navigation module
const SlideWrapper = ({ itemSlide }: any) => {
  return (
    <>
      {itemSlide.length > 0 ? (
        <>
          <div className="hero-slide">
            <Swiper
              
              modules={[Autoplay, Navigation]}
              centeredSlides={true}
              spaceBetween={0}
              slidesPerView={1}
              navigation={{
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
              }}
              className="mySwiper"
              // autoplay={{delay: 3000}}
            >
              <div className="swiper-button-next"></div>
        <div className="swiper-button-prev"></div>
              {itemSlide.map((item: any, i: any) => (
                <SwiperSlide key={i}>
                  {({ isActive }) => (
                    <HeroSlideItem
                      item={item}
                      className={`${isActive ? "active" : ""}`}
                    />
                  )}
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </>
      ) : (
        <>
          <h3
            style={{
              fontSize: "16px",
              paddingTop: 10,
              borderTop: "1px solid #eee",
            }}
            className="text-center title"
          >
            Không có dữ liệu hình ảnh
          </h3>
        </>
      )}
    </>
  );
};
const HeroSlideItem = (props: any) => {
  const item = props.item;
  return (
    <img src={item.image} alt="img-slide" />
  );
};
export default SlideWrapper;
