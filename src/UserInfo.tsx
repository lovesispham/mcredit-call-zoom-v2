import React, {useState} from 'react'
import axios from "axios";
const UserInfo = () => {
    const [showMsg, setShowMsg] = useState<boolean>(false)
    const [msgStatus, setMsgStatus] = useState<string>('false')
    const [msgText, setMsgText] = useState<string>('')
    const [newvalue, setValue] = useState<{
        old_password: string;
        new_password: string;
        retype_password: string;
      }>({
        old_password: '',
        new_password:  '',
        retype_password: '',
      });

      const handleChangeInput = (e: any) => {
        const { name, value } = e.target;
        setValue({ ...newvalue, [name]: value });
      };


      let token = localStorage.getItem("access_token");
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      };

      const handleChangePassword = async() => {
        
          axios
            .post(`${process.env.REACT_APP_API_URL}api/change-password/`, newvalue, axiosConfig)
            .then((res) => {
                if (res.status === 200) {
                    setMsgStatus(res.data.error);
                    setMsgText('Đổi mật khẩu thành công');
                    setShowMsg(true);
                  } 
                  
            })
            .catch((error=>{
              setMsgStatus('false');
              setMsgText('Sai mật khẩu');
              setShowMsg(true);
            }))
      }

  return (
    <div className="container">
        <div className="user-info">
            {
              showMsg && (
                <div className={`alert ${msgStatus === 'false' ? 'alert-danger' : 'alert-success'}`} role="alert">
                  {msgText}
                </div>
              )
             
              
            }
            <div className="form-input">
                <span className="label-name">Mật khẩu cũ</span>
                <input 
                value={newvalue.old_password} 
                onChange = {handleChangeInput}
                type="password" className="form-control" name='old_password' placeholder='Mật khẩu cũ' />
            </div>
            <div className="form-input">
                <span className="label-name">Mật khẩu mới</span>
                <input 
                value={newvalue.new_password} 
                onChange = {handleChangeInput} 
                type="password" className="form-control" name='new_password' placeholder='Mật khẩu mới' />
            </div>
            <div className="form-input">
                <span className="label-name">Nhập lại mật khẩu</span>
                <input 
                value={newvalue.retype_password} 
                onChange = {handleChangeInput} 
                type="password" className="form-control" name='retype_password' placeholder='Nhập lai mật khẩu' />
            </div>
            <div className="form-input text-right">
                <div className="btn" 
                onClick={() => handleChangePassword()}
                >
                Thay đổi mật khẩu
                </div>
            </div>
        </div>
    </div>
  )
}

export default UserInfo