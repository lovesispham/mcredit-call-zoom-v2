import Footer from "./components/Footer";
import Header from "./components/Header";
import User from "./UserInfo";
const Employee2 = () => {
  return (
    <>
      <Header />
      <div>
        <User />
      </div>
      {<Footer />}
    </>
  );
};

export default Employee2;
