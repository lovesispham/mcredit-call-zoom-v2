import Footer from "./components/Footer";
import Header from "./components/Header";
import ZoomTest from "./ZoomTest";
const Employee2 = () => {
  return (
    <>
      <Header />
      <div>
        <ZoomTest />
      </div>
      {<Footer />}
    </>
  );
};

export default Employee2;
