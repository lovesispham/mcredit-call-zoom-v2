/** @format */

import React from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

import Test from "./Employee2";
import Login from "./Login";
import Report from "./Report";
import ReportChart from "./ReportChart";
import UserPage from "./UserPage";
import { ProtectedRoute } from "./protectedRouter";
import ZoomTest3 from "./ZoomTest3";
function App() {
  const token = localStorage.getItem("access_token");

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={token ? Test : Login} />
          <Route path="/login" component={Login} />
          <ProtectedRoute path="/staff" component={Test} />
          <Route path="/staff3" component={ZoomTest3} />
          <ProtectedRoute path="/report" component={Report} />
          <ProtectedRoute path="/info" component={UserPage} />
          <ProtectedRoute path="/report-table" component={ReportChart} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
