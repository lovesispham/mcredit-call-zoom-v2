/** @format */
import "./index.css";
import { JitsiMeeting } from "@jitsi/react-sdk";

export const Jisti = () => {
  return (
    <JitsiMeeting
      // domain={YOUR_DOMAIN}
      roomName="vntel123"
      configOverwrite={{
        startWithAudioMuted: true,
        disableModeratorIndicator: false,
        startScreenSharing: true,
        enableEmailInStats: false,
        prejoinPageEnabled: false,
      }}
      interfaceConfigOverwrite={{
        DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
        SHOW_CHROME_EXTENSION_BANNER: false,
      }}
      userInfo={{
        displayName: "Gonzalo",
        email: "thanhnd.bkhn@gmail.com",
      }}
      // onApiReady={(externalApi) => {
      //   // here you can attach custom event listeners to the Jitsi Meet External API
      //   // you can also store it locally to execute commands
      // }}
      getIFrameRef={(iframeRef) => {
        iframeRef.style.height = "400px";
        iframeRef.style.width = "800px";
      }}
    />
  );
};
