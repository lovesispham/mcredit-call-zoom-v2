const urlApi = {
    getToken: `${process.env.REACT_APP_API_URL}staff-api/get-token-bpm/`,
    getFace : `${process.env.REACT_APP_API_URL}api/get-face-match-htc/`,
    getHistory: `${process.env.REACT_APP_API_URL}api/history`,
    getFeedback:`${process.env.REACT_APP_API_URL}api/feedback/`,
    getExportChart: `${process.env.REACT_APP_API_URL}api/export/chart/`,
    createFeedback: `${process.env.REACT_APP_API_URL}api/feedback/`,
    createToken:`${process.env.REACT_APP_API_URL}api/mytoken/`,
    getDownloadHistory:`${process.env.REACT_APP_API_URL}api/download/history`,
    getDataCloud:`${process.env.REACT_APP_API_URL}api/history-cloud/`,
    createPassword:`${process.env.REACT_APP_API_URL}api/change-password/`,
    signatureEndpoint:`${process.env.REACT_APP_API_URL}staff-api/create`,
    sendSms:`${process.env.REACT_APP_API_URL}staff-api/send-sms`,
    updateRecord:`${process.env.REACT_APP_API_URL}api/update-cdr?`,
    updateActiveRecord:`${process.env.REACT_APP_API_URL}api/active-record/`,
    getAddress:`${process.env.REACT_APP_API_URL}staff-api/address/`,
    getAttachments:`${process.env.REACT_APP_API_URL}api/upload/`,
    createUser:`${process.env.REACT_APP_API_URL}staff-api/user/`,
    getInfoPhone:`${process.env.REACT_APP_API_URL}api/get-profile-htc/`,
    sendMcNoti:`${process.env.REACT_APP_API_URL}api/send-mc-noti/`,
    sendMcNotiTest:`${process.env.REACT_APP_API_URL}api/send-mc-noti-test/`,
}

export default urlApi


