import React,{useState} from "react";
import { saveAs } from 'file-saver'
const TableImg = ({ dataImg }: any) => {
  const [openModal, setOpenModal] = useState<boolean>(false)
  const [itemImg, setItem] = useState<any>('')
  const handleOpenModal = () => {
    setOpenModal(true);
    
  }

  const handleCloseModal = () => {
    setOpenModal(false);
  };
   
  const handleDowloadImg = (item:any) => {
    saveAs(item)
  }

  return (
    <div className="table-attachments">
      <div className="table-responsive">
        {
            dataImg.length > 0 ? 
            <>
            {
            openModal && (
                <>
                <div className="modal-overlay" onClick={() => handleCloseModal()}></div>
          <div className="modal-popup_style2 open">
            
            <div className="popup-container pu-video">
              <div className="popup-close" onClick={() => handleCloseModal()}>
                <span className="fa fa-close"></span>
              </div>
              <div className="content">
                <span className="btn btn-warning"
                  onClick={() => handleDowloadImg(itemImg)}
                >
                  <i className="fa fa-download"></i>
                </span>
                <img src={itemImg} alt="img" />
              </div>
            </div>
          </div>
        </>
            )
        }
             <table className="table">
          <thead>
            <tr>
              <th>STT</th>
              <th>Tệp ảnh</th>

              <th>Ngày tạo</th>
            </tr>
          </thead>
          <tbody>
            {dataImg &&
              dataImg.sort((a:any, b:any) => (a.created_on < b.created_on ? 1 : -1))
              .map((item: any, index: number) => {
                return (
                  <tr key={index}>
                    <th style={{ textAlign: "center" }}>{index + 1} </th>
                    <th>
                      <div className="photo-img"
                      onClick={()=>{
                        handleOpenModal()
                        setItem(item?.image)
                        }}
                      >
                          <img alt="img" src={item?.image} />
                      </div>
                      
                    </th>

                    <th>{item?.create_date} </th>
                  </tr>
                );
              })}
          </tbody>
        </table>
            </> : 
            <>
            <h3 style={{ fontSize: "16px", paddingTop:10,borderTop:'1px solid #eee' }} className="text-center title">Không có dữ liệu hình ảnh</h3>
            </>
        }
       
      </div>
    </div>
  );
};

export default TableImg;
