import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const Chart = () => {
  const [dataChart, setDataChart] = useState<any>("");
  let token = localStorage.getItem("access_token");
  let axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  };
  var today = new Date();

  const [exportData, setExportData] = useState<{
    start: Date | any;
    end: Date | any;
    status: string;
  }>({
    start: new Date(today.getFullYear(), today.getMonth(), 1),
    end: new Date(today.getFullYear(), today.getMonth() + 1, 0),
    status: "Tốt",
  });

  const handleChangeExport = (date: any, name: any) => {
    setExportData({
      ...exportData,
      [name]: date,
    });
  };

  const onChange = (e: any) => {
    const { name, value } = e.target;
    setExportData({ ...exportData, [name]: value });
  };

  const [dataFeed, setDataFeed] = useState<any>("");

  const getFeedStatus = async () => {
    axios
      .get(`${process.env.REACT_APP_API_URL}api/feedback/`, axiosConfig)
      .then((res) => {
        setDataFeed(res.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getChart = async () => {
    let params = "";
    params +=
      exportData.start !== null && exportData.start !== undefined
        ? `start=${moment(exportData.start).format("YYYY-MM-DD")}&`
        : "";
    params +=
      exportData.end !== null && exportData.end !== undefined
        ? `end=${moment(exportData.end).format("YYYY-MM-DD")}&`
        : "";
    params +=
      exportData.status !== null && exportData.status !== undefined
        ? `status=${exportData.status}&`
        : "";
    axios
      .get(
        `${process.env.REACT_APP_API_URL}api/export/chart/?${params}`,
        axiosConfig
      )
      .then((res) => {
        console.log(res);

        setDataChart(res?.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getFeedStatus();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getChart();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [exportData.status]);

  const getFeedData = () => {
    const dataArr = {
      labels: [] as any,
      count: [] as any,
    };
    dataChart?.data?.map((item: any) => {
      dataArr.labels.push(item?.date);
      dataArr.count.push(item?.rate);
      return dataArr;
    });
    return dataArr;
  };

  const dataFeedChart = {
    labels: getFeedData().labels,
    datasets: [
      {
        label: exportData.status,
        data: getFeedData().count,
        backgroundColor: "rgba(90, 141, 238, 0.85)",
        borderColor: "rgba(90, 141, 238, 0.85)",
      },
    ],
  };

  return (
    <div className="container">
      <div className="form-chart">
        <div className="form-input">
          <span className="label-name">Ngày bắt đầu - Ngày kết thúc</span>
          <div className="box-date">
            <DatePicker
              autoComplete="off"
              id="from"
              name="start"
              selected={exportData.start}
              dateFormat="dd/MM/yyyy"
              onChange={(date) => handleChangeExport(date, "start")}
            />
            <DatePicker
              autoComplete="off"
              id="to"
              name="end"
              selected={exportData.end}
              dateFormat="dd/MM/yyyy"
              onChange={(date) => handleChangeExport(date, "end")}
            />
          </div>
        </div>

        <div className="form-input">
          <div className="label-name">Chọn nội dung đánh giá</div>
          <select
            className="form-select"
            name="status"
            value={exportData.status}
            onChange={onChange}
          >
            {dataFeed &&
              dataFeed.map((item: any, index: number) => {
                return (
                  <option key={index} value={item.name}>
                    {item.name}
                  </option>
                );
              })}
          </select>
        </div>

        <div className="form-input text-right">
          <div className="btn" onClick={() => getChart()}>
            Phân tích dữ liệu
          </div>
        </div>
      </div>
      {dataChart && (
        <Line
          data={dataFeedChart}
          options={{
            responsive: true,
            scales: {
              y: {
                  min: 0,
                  max: 100,
                  ticks: {
                      stepSize: 20,
                      callback: function(value, index, values) {
                          return value + " %";
                      }            
                  }
              }
          },
            plugins: {
              title: {
                display: true,
                text: "Biểu đồ phân tích dữ liệu",
                font: {
                  size: 20,
                },
              },
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset.label || "";
                    if (context.parsed.y !== null) {
                      label += " " + context.parsed.y + "%";
                    }
                    return label;
                  }
                }
              },
              legend: {
                display: true,
                position: "top",
              },
            },
          }}
        />
      )}
    </div>
  );
};

export default Chart;
