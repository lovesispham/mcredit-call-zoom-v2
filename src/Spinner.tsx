import React from "react";

const Spinner = ({ heading, percentText }: any) => {
  return (
    <div id="upload-loading" className="loading">
      <div className="box-text-loading">
        {heading && <h3 className="title text-center" style={{fontSize:28}}>{heading}</h3>}
        
        <div className="percent-loading">{percentText}
          {percentText && '%'}
        </div>
        
        <i style={{ marginTop: 50 }} className="spinning-loader"></i>
      </div>
    </div>
  );
};

export default Spinner;
