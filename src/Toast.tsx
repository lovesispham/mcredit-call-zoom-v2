import React, { useEffect } from "react";
const Toast = ({ type, message, loading, data, setLoading2 }: any) => {
  
  useEffect(() => {
    setTimeout(() => {
      
      setLoading2({
        message: '', 
        show: false
      })
    }, 3000);
    //eslint-disable-next-line
  }, [loading]);



  return (
    <>
      {loading && (
        <div
          className={`notifi custom-toast bottom-left ${
            type === "success" ? "bg-success" : "bg-error"
          }`}
        >
          <div className="notification-image">
            {type === "success" ? (
              <>
                <i className="fa fa-check-circle"></i>
              </>
            ) : (
              <>
                <i className="fa fa-times-circle"></i>
              </>
            )}
          </div>
          <div>
            <p className="notification-title">
              {type === "success" ? "Thành công" : "Lỗi"}
            </p>
            <p className="notification-message">{message || data?.errorType}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default Toast;
