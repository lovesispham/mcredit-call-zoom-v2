import React, { useEffect, useState } from "react";
import axios from "axios";
import { useHistory, useLocation } from "react-router-dom";
import ZoomMtgEmbedded from "@zoomus/websdk/embedded";
const Feedback = ({uuidZoom,handleEndMeeting, code}: any) => {
  const history = useHistory();
  let location = useLocation();
  const pathname = location.pathname;
  const [dataFeed, setDataFeed] = useState<any>("");


  const [dataSubmit, setDataSubmit] = useState<{
    code:string;
    status: string;
    description:string;
    uuid:string;
  }>({
    code:code,
    status:'1',
    description:'',
    uuid:uuidZoom
  });


  let token = localStorage.getItem("access_token");
  let axiosConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  };

  const getFeedStatus = async () => {
    axios
      .get(`${process.env.REACT_APP_API_URL}api/feedback/`, axiosConfig)
      .then((res) => {
        setDataFeed(res.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getFeedStatus();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const handleChangeInput = (e: any) => {
    const { name, value } = e.target;
    setDataSubmit({ ...dataSubmit, [name]: value });
  };

  const onChange = (e:any) => {
    const { name, value } = e.target;
    setDataSubmit({ ...dataSubmit, [name]: value });
  }

  const handleSubmitFeed = () => {
    
    axios
      .post(
        `${process.env.REACT_APP_API_URL}api/feedback/`,
        dataSubmit,
        axiosConfig
      )
      .then((res) => {
        // console.log(res)
        handleEndMeeting()
        ZoomMtgEmbedded.destroyClient()
        if(pathname === '/staff'|| pathname === '/'){
          window.location.reload();
        }
        if(pathname === '/staff3'){
          history.push('/')
        }
      })
      .catch((error) => {
        console.error(error);
        handleEndMeeting()
        ZoomMtgEmbedded.destroyClient()
        if(pathname === '/staff' || pathname === '/'){
          window.location.reload();
        }
        if(pathname === '/staff3'){
          history.push('/')
        }
      });
  };
  return (
    <>
      <div className="modal-overlay"></div>
      <div className="modal-popup_style2 open">
        <div className="popup-container pu-feedback">
          <div className="popup-close">
            <span className="fa fa-close"></span>
          </div>
          <div className="content">
            <div className="box-form">
            <div className="form-input">
                <div className="label-name">Mã số</div>

                <input
                  onChange={handleChangeInput}
                  name="code"
                  className="form-control"
                  value={dataSubmit.code}
                ></input>
              </div>
              <div className="form-input">
                <div className="label-name">Chọn nội dung đánh giá</div>
                <select className="form-select"
                name="status"
                value={dataSubmit.status}
                onChange={onChange}>
                
                  {dataFeed &&
                    dataFeed.map((item: any, index: number) => {
                      return <option key={index} value={item.id}>{item.name}</option>;
                    })}
                </select>
              </div>
              <div className="form-input">
                <div className="label-name">Nội dung</div>

                <textarea
                  onChange={handleChangeInput}
                  name="description"
                  className="form-control"
                  value={dataSubmit.description}
                ></textarea>
              </div>
              <div className="form-input text-right">
                <div className="btn" onClick={()=>handleSubmitFeed()}>Gửi</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Feedback;
