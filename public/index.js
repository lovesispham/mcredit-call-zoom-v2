const ScreenCapture = {
  data: {
    crossHairsLeft: 0,
    crossHairsTop: 0,
    startX: 0,
    startY: 0,
    endX: 0,
    endY: 0,
    isMouseDown: false,
    borderWidth: 0,
    windowWidth: 0,
    windowHeight: 0,
    cropPositionTop: 0,
    cropPositionLeft: 0,
    cropWidth: 0,
    cropHeight: 0,
    imageUrl: "",
  },

  handleWindowResize: function () {
    const windowWidth =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    const windowHeight =
      window.innerHeight ||
      document.documentElement.clientHeight ||
      document.body.clientHeight;

    this.data.windowWidth = windowWidth;
    this.data.windowHeight = windowHeight;
  },

  move: function (event) {
    const {
      isMouseDown,
      windowWidth,
      windowHeight,
      startX,
      startY,
      borderWidth,
    } = this.data;
    let cropPositionTop = startY;
    let cropPositionLeft = startX;

    const endX = event.clientX;
    const endY = event.clientY;
    const isStartTop = endY >= startY;
    const isStartBottom = endY <= startY;
    const isStartLeft = endX >= startX;
    const isStartRight = endX <= startX;
    const isStartTopLeft = isStartTop && isStartLeft;
    const isStartTopRight = isStartTop && isStartRight;
    const isStartBottomLeft = isStartBottom && isStartLeft;
    const isStartBottomRight = isStartBottom && isStartRight;
    let newBorderWidth = borderWidth;
    let cropWidth = 0;
    let cropHeight = 0;


    if (isMouseDown) {
      if (isStartTopLeft) {
        newBorderWidth = `${startY}px ${windowWidth - endX}px ${windowHeight - endY
          }px ${startX}px`;
        cropWidth = endX - startX;
        cropHeight = endY - startY;
      }

      if (isStartTopRight) {
        newBorderWidth = `${startY}px ${windowWidth - startX}px ${windowHeight - endY
          }px ${endX}px`;
        cropWidth = startX - endX;
        cropHeight = endY - startY;
        cropPositionLeft = endX;
      }

      if (isStartBottomLeft) {
        newBorderWidth = `${endY}px ${windowWidth - endX}px ${windowHeight - startY
          }px ${startX}px`;
        cropWidth = endX - startX;
        cropHeight = startY - endY;
        cropPositionTop = endY;
      }

      if (isStartBottomRight) {
        newBorderWidth = `${endY}px ${windowWidth - startX}px ${windowHeight - startY
          }px ${endX}px`;
        cropWidth = startX - endX;
        cropHeight = startY - endY;
        cropPositionLeft = endX;
        cropPositionTop = endY;
      }
    }

    cropWidth *= window.devicePixelRatio;
    cropHeight *= window.devicePixelRatio;

    this.data = {
      ...this.data,
      crossHairsTop: event.clientY,
      crossHairsLeft: event.clientX,
      borderWidth: newBorderWidth,
      cropWidth,
      cropHeight,
      cropPositionTop: cropPositionTop,
      cropPositionLeft: cropPositionLeft,
    }
  },
  mouseDown: function (event) {
    const startX = event.clientX;
    const startY = event.clientY;

    this.data = {
      ...this.data,
      startX,
      startY,
      cropPositionTop: startY,
      cropPositionLeft: startX,
      isMouseDown: true,
      borderWidth: `${this.data.windowWidth}px ${this.data.windowHeight}px`,
    }
  },

  mouseUp: function (e) {
    this.takeScreenshot();
    this.data.borderWidth = 0; // resetting the overlay
    this.data.mouseIsDown = false;
  },

  takeScreenshot: function () {
    const { cropPositionTop, cropPositionLeft, cropWidth, cropHeight } =
      this.data;
    const body = document.querySelector("body");

    if (body) {
      // eslint-disable-next-line no-undef
      html2canvas(body).then((canvas) => {
        const croppedCanvas = document.createElement("canvas");
        const croppedCanvasContext = croppedCanvas.getContext("2d");

        croppedCanvas.width = cropWidth;
        croppedCanvas.height = cropHeight;

        if (croppedCanvasContext) {
          croppedCanvasContext.drawImage(
            canvas,
            cropPositionLeft,
            cropPositionTop,
            cropWidth,
            cropHeight,
            0,
            0,
            cropWidth,
            cropHeight
          );
        }

        const urlData = croppedCanvas.toDataURL();
        this.data.imageUrl = urlData;
      });
    }
  }
}

let screenCaptureEl;
let overlayEl;
let crossHairsEl;

window.addEventListener("resize", ScreenCapture.handleWindowResize());


window?.addEventListener("mousedown", function (e) {
  e?.path?.forEach((element) => {
    if (
      element instanceof HTMLElement &&
      element?.id?.includes(
        `button-capture-id`
      )
    ) {
      const buttonCaptureEl = document.getElementById('button-capture-id');
      buttonCaptureEl?.addEventListener("click", function () {
        screenCaptureEl = document.getElementsByTagName("BODY")[0];
        overlayEl = document.getElementById('overlay-id');
        crossHairsEl = document.getElementById('crosshairs-id')

        overlayEl.style.display = 'block';
        crossHairsEl.style.display = 'block';
        screenCaptureEl.addEventListener('mousemove', mouseMove);
        screenCaptureEl.addEventListener('mousedown', mouseDown);
        screenCaptureEl.addEventListener('mouseup', mouseUp);
      })
    }
  });

})

function mouseDown(e) {
  ScreenCapture.mouseDown(e);
  overlayEl.classList.add("highlighting");
  overlayEl.style.borderWidth = ScreenCapture.data.borderWidth;
}

function mouseMove(e) {
  ScreenCapture.move(e);
  crossHairsEl.style.left = `${ScreenCapture.data.crossHairsLeft}px`;
  crossHairsEl.style.top = `${ScreenCapture.data.crossHairsTop}px`;


  if (ScreenCapture.data.isMouseDown) {
    overlayEl.style.borderWidth = ScreenCapture.data.borderWidth;
  } else {
    overlayEl.style.borderWidth = `0`;
  }

}

function mouseUp(e) {
  ScreenCapture.mouseUp(e);
  overlayEl.classList.remove("highlighting");
  overlayEl.style.display = 'none';
  crossHairsEl.style.display = 'none';
  overlayEl.style.borderWidth = ScreenCapture.data.borderWidth;
  setTimeout(() => {
    if (ScreenCapture.data.imageUrl !== "") {
      console.log('here');
      modal.style.display = "block";
      const imageEl = document.getElementById('image-id');
      imageEl.src = ScreenCapture.data.imageUrl;
    }
  }, 1000)

  screenCaptureEl.removeEventListener("mousemove", mouseMove);
  screenCaptureEl.removeEventListener("mousedown", mouseDown);
  screenCaptureEl.removeEventListener('mouseup', mouseUp);
}


// for action of modal
var modal = document.getElementById('myModal');

var span = document.getElementsByClassName("close")[0];

if (span) {
  span.onclick = function () {
    modal.style.display = "none";
  }
}

window.onclick = function (event) {
  if (event.target === modal) {
    modal.style.display = "none";
  }
}

// for download, copy image

document.getElementById('cancel-id')?.addEventListener('click', function () { modal.style.display = "none"; });
document.getElementById('download-id')?.addEventListener('click', function () {
  const downloadLink = document.createElement("a");
  const fileName = "image-customer.png";

  if (ScreenCapture.data.imageUrl) {
    downloadLink.href = ScreenCapture.data.imageUrl;
    downloadLink.download = fileName;
    downloadLink.click();
  }
})

document.getElementById('copy-id')?.addEventListener('click', async function () {
  if (ScreenCapture.data.imageUrl) {
    const img = await fetch(ScreenCapture.data.imageUrl);
    const imgBlob = await img.blob();
    copyToClipboard(imgBlob);
  }
})

async function copyToClipboard(pngBlob) {
  try {
    await navigator.clipboard.write([
      // eslint-disable-next-line no-undef
      new ClipboardItem({
        [pngBlob.type]: pngBlob,
      }),
    ]);
    console.log("Image copied");
  } catch (error) {
    console.error(error);
  }
}

// for display video stream